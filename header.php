

<html>
<head>
    <title> project</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" >



</head>
<body>
<div>
    <nav class="navbar navbar-inverse">

        <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">contact</a></li>
            <li><a href="views/auth/create.php">Sign Up</a></li>
            <?php
            if(empty($_SESSION['userid']))
            { ?>

                <li><a href="login.php">Login</a></li>

            <?php } ?>

            <?php
            if(!empty($_SESSION['userid']))
            { ?>

            <li><a href="logout.php">logout</a></li>

            <?php } ?>

            <?php
            error_reporting(0);
            //session_start();
            if($_SESSION['userid']['is_admin']==1)
            { ?>
                <li><a href="admin/index.php">Admin</a></li>

         <?php }?>
        </ul>

        <form class=" navbar-form navbar-right">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="search"/>
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>

                    </button>
                </div>
            </div>
        </form>

    </nav>