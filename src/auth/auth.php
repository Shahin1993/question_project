<?php

namespace App\auth;
//include("../../vendor/autoload.php");

use App\utility\utility;
use PDO;
session_start();
class auth
{
    private $username;
    private $email;
    private $password;

    public function setData($data = '')
    {
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->password = $data['password'];
        return $this;
    }

    public function store()
    {
        try {
            $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
            $query = "INSERT INTO user_tbl(username,email,password,create_at) values(:u, :e, :p, :c)";
            $stmt = $db->prepare($query);
            $status = $stmt->execute(
                array(
                    ':u' => $this->username,
                    ':e' => $this->email,
                    ':p' => $this->password,
                    ':c' => date('Y-m-d h:m:s'),
                ));
            if ($status) {
                $_SESSION['Message'] = "<div class='success'>Sign up successfully.</div>";

                header("location:create.php");
            } else {
                echo "something is wrong";
            }

        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }

    public function getusers()
    {
        try{

            $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
            $query = "SELECT * FROM `user_tbl`  ";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
        }
        catch (PDOException $e)
        {
            echo 'Error' . $e->getMessage();
        }
        return $data;

    }
    public function login($data='')
    {
       $this->username="'".$data['username']."'";
       $this->password="'".$data['password']."'";

        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $query = "SELECT * FROM `user_tbl` WHERE username=$this->username AND password=$this->password";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        if(!empty($data))
        {
            $_SESSION['userid']=$data;
            header("location:../index.php");
        }
        else{
            header("location:../login.php");
            $_SESSION['Message']="<div class='error'>Opps! Username or Password not matching</div>";

        }
    }

    public function admin_login($data2='')
    {
        $this->username="'".$data2['username']."'";
        $this->password="'".$data2['password']."'";

        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $query = "SELECT * FROM `tbl_admin` WHERE username=$this->username AND password=$this->password";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        if(!empty($data))
        {
            $_SESSION['userid']=$data;
            header("location:../admin/index.php");
        }
        else{
            $_SESSION['Message']="<div class='error'>Opps!is only adimin ....</div>";
            header("location:../admin/login.php");
        }
    }

    public function view($id='')
    {

        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $query = "SELECT * FROM `user_tbl` WHERE id=$id";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function delete($id='')
    {
        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $statement= $db->prepare("DELETE FROM user_tbl WHERE id=?");
        $statement->execute(array($id));

        header("location: view_users.php");
    }

    public function show($id='')
    {

        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $query = "SELECT * FROM `tbl_question` WHERE id=$id";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function q_delete($id='')
    {
        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $statement= $db->prepare("DELETE FROM tbl_question WHERE id=?");
         $status=$statement->execute(array($id));

        if ($status) {
            $_SESSION['Message'] = "<div class='success'>Deleted  successfully.</div>";

            header("location: question_view.php");
        }

    }


}
?>